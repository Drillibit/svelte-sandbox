const svelteConfig = require('./svelte.config');

module.exports = {
  transform: {
    '^.+\\.(ts|js)$': 'babel-jest',
    '^.+\\.svelte$': [
      'svelte-jester',
      {
        preprocess: true,
      },
    ],
  },
  setupFilesAfterEnv: ['@testing-library/jest-dom/extend-expect'],
  moduleFileExtensions: ['ts', 'js', 'svelte'],
};
